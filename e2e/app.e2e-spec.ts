import { FtoolPage } from './app.po';

describe('ftool App', () => {
  let page: FtoolPage;

  beforeEach(() => {
    page = new FtoolPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
