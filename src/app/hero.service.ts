import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Hero } from './datas';
// import { HEROES } from './mock-heroes';



@Injectable()
export class HeroService {
  private headers = new Headers({ 'Content-Type': 'application/json' });
  // private heroesUrl = 'http://go.agency911.org/api/showjson';  // URL to web api
  private heroesUrl = 'http://go.agency911.org/api/jsontoui';  // URL to web api
  constructor(private http: Http) {

  }

  getHeroes(): Promise<Hero[]> {

    return this.http.get(this.heroesUrl)
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }






  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
