import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Hero } from './datas';
import { HeroService } from './hero.service';
// import { DemoPaginationBasicComponent} from './pagination.component';


@Component({
  selector: 'app-example',
  template: `
    <!-- <form #f="ngForm" (ngSubmit)="onSubmit(f)" novalidate>
       <input name="first" ngModel required #first="ngModel">
      <input name="last" ngModel>
      <button>Submit</button>
    </form>
    <p>First name value: {{ first.value }}</p>
    <p>first name valid: {{ first.valid }}</p>
    <p>Form value: {{ f.value | json }}</p>
    <p>Form valid: {{ f.valid }}</p> !-->
<p>heroes</p>
    <h2>My Heroes</h2>

    <ul class="heroes list-unstyled">
      <li *ngFor="let hero of heroesKeys
       | count : paginationCallback |  paginate : currentPage : itemsPerPage"
        [class.selected]="heroes[hero] === selectedHero"
        (click)="onSelect(heroes[hero])">
        <span class="badge">{{heroes[hero].Alkohal_Gram}}</span> {{heroes[hero].name}}
      </li>

    </ul>
    <pagination [boundaryLinks]="true" [totalItems]="totalItems" [(ngModel)]="currentPage" class="pagination-sm"
           (pageChanged)="currentPage = $event.page" previousText="&lsaquo;" nextText="&rsaquo;" firstText="&laquo;"
           lastText="&raquo;" [maxSize]="paginationMaxSize"></pagination>

    <hero-detail [hero]="selectedHero"></hero-detail>
  `,
  providers: [HeroService]
})
export class SimpleFormComponent implements OnInit {
  heroes: Hero[];
  selectedHero: Hero;
  totalItems: number;
  heroesKeys: string[];
  private currentPage: number;
  private itemsPerPage: number;
  private paginationMaxSize: number;
  private smallnumPages: number;
  private paginationCallback: (number) => void;
  private setPage(pageNo: number): void {
    this.currentPage = pageNo;
  }
  constructor(private heroService: HeroService) { }
  getHeroes(): void {
    this.heroService.getHeroes().then(heroes => {
      this.heroes = heroes;
      this.heroesKeys = Object.keys(heroes);
      this.totalItems = heroes.length;
    });
  }

  ngOnInit(): void {
    this.currentPage = 1;
    this.totalItems = 1;
    this.itemsPerPage = 10;
    this.paginationMaxSize = 10;

    this.getHeroes();
    this.paginationCallback = (numberOfItems: number) => {
      this.totalItems = numberOfItems;
    };
  }

  public pageChanged(event: any): void {
    console.log('Page changed to: ' + event.page);
    console.log('Number items per page: ' + event.itemsPerPage);
    this.setPage(event.page);

  }

  onSelect(hero: string): void {
    this.selectedHero = this.heroes[hero];
    this.paginationCallback = (numberOfItems: number) => {
      this.totalItems = numberOfItems;
    };
  }
  onSubmit(f: NgForm) {
    console.log(f.value);  // { first: '', last: '' }
    console.log(f.valid);  // false
  }

}
