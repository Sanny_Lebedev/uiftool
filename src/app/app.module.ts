import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { SortableModule } from 'ngx-bootstrap/sortable';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { AppComponent } from './app.component';
import { DemoPaginationBasicComponent} from './pagination.component';
import { SimpleItemsDemoComponent} from './simple-items.component';
import { FormsModule } from '@angular/forms';
import {SimpleFormComponent} from './test.component';
import { HeroDetailComponent } from './hero-detail.component';

import { PaginatePipe } from './paginatepipe.service';
import { CountPipe } from './countpipe.service';

@NgModule({
  declarations: [
    AppComponent,
    PaginatePipe,
    CountPipe,
    SimpleItemsDemoComponent,
    SimpleFormComponent,
    HeroDetailComponent,
    DemoPaginationBasicComponent

  ],
  imports: [
    BrowserModule,
    SortableModule.forRoot(),
    PaginationModule.forRoot(),
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
